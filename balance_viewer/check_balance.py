#! env python
# -*- coding: utf-8 -*-
import argparse
import re
from time import sleep

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.firefox.options import Options


def main():
    args = parse_args()
    validate(args)
    balance = check_balance(args)
    print_result(balance)


def parse_args():
    parser = argparse.ArgumentParser(description="Утилита для проверки баланса интернет-провайдера velcom")
    parser.add_argument("-a", "--account", help="Лицевой счёт")
    parser.add_argument("-p", "--password", help="Пароль")
    return parser.parse_args()


def validate(args) -> None:
    if not args.account:
        exit("Не найден лицевой счёт.")
    if not args.password:
        exit("Не найден пароль.")
    if not re.match(r"^\d+$", args.account):
        exit("Лицевой счёт должен состоять только из цифр.")


def check_balance(args):
    try:
        checker = BalanceChecker(args.account, args.password)
        checker.login()
        checker.parse_balance()
    finally:
        checker.driver.quit()
        return checker.balance


def print_result(balance) -> None:
    message = "Не удалось проверить баланс."
    if balance:
        message = "Баланс: {} руб.".format(balance)
    print(message)


class BalanceChecker:

    def __init__(self, account, password, check_interval=.2, timeout=15):
        self.account = account
        self.password = password
        self.check_interval = check_interval
        self.timeout = timeout
        self.balance = ""
        self.options = self.set_options()
        self.driver = webdriver.Firefox(options=self.options)
        self.login_page_url = ("https://asmp.velcom.by/asmp/LoginMasterServlet"
                               "?userRequestURL=https%253A%252F%252Fmy.velcom.by%252F"
                               "&serviceRegistrationURL=&service=ISSA"
                               "&wrongLoginType=false&cookie=skip&level=30#2")

    def set_options(self):
        options = Options()
        options.add_argument("--headless")
        return options

    def login(self):
        self.driver.get(self.login_page_url)
        i = 0
        while i < self.timeout:
            try:
                form = self.driver.find_element_by_tag_name("form")
                account_field = form.find_element_by_name("UserIDFixed")
                password_field = form.find_element_by_name("fixedPassword")
                login_button = form.find_element_by_tag_name("button")
                break
            except NoSuchElementException:
                i += self.check_interval
                sleep(self.check_interval)
        else:
            exit("Не удалось проверить баланс.")
        account_field.send_keys(self.account)
        password_field.send_keys(self.password)
        login_button.submit()

    def parse_balance(self):
        i = 0
        while i < self.timeout:
            try:
                raw_balance = self.driver.find_element_by_id("BALANCE").text
                break
            except NoSuchElementException:
                i += self.check_interval
                sleep(self.check_interval)
        else:
            raw_balance = ""
        match = re.search(r"(?P<major>(?:-\s+)?\d+)\s+руб\.\s+(?P<minor>\d+)\sкоп\.", raw_balance)
        if match:
            self.balance = "{}.{}".format(match["major"].replace(" ", ""), match["minor"])


if __name__ == '__main__':
    main()
