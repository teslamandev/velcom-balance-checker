APP_PATH=./balance_viewer
TEST_PATH=./tests

.PHONY: clean test help

clean:
	find -name '*.pyc' -delete
	find -name '*.swp' -delete
	find -name '__pycache__' -delete
	find -name 'geckodriver.log' -delete

test: clean
	poetry run python -m pytest --color=yes $(TEST_PATH)

help:
	@echo "    clean"
	@echo "        remove runtime files"
	@echo "    test"
	@echo "        run tests"

